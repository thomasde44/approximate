from cmath import sqrt
from numpy import random
import numpy as np

nu = random.uniform(size=(1, 500))
mu = random.uniform(size=(1, 500))


cmplx = []


# N = 500
cnt = 0
for i in range(500-1):
    cmplx.append(complex(nu[0][i],mu[0][i]))
    if 1 >= abs(cmplx[i]):
        cnt += 1


# N = 5000
pi = 4*(cnt/500)
print(pi)

nu2 = random.uniform(size=(1, 5000))
mu2 = random.uniform(size=(1, 5000))


cmplx2 = []

cnt2 = 0
for i in range(5000-1):
    cmplx2.append(complex(nu2[0][i],mu2[0][i]))
    if 1 >= abs(cmplx2[i]):
        cnt2 += 1



pi2 = 4*(cnt2/5000)
print(pi2)
  